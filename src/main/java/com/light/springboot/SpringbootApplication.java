package com.light.springboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;

/**
该注解指定项目为springboot，由此类当作程序入口
自动装配 web 依赖的环境

**/

/**
 * 打包成部署的 war 包, "mvn clean package"
 * 让 SpringbootApplication 类继承 SpringBootServletInitializer 并重写 configure 方法
 */
@SpringBootApplication
public class SpringbootApplication extends SpringBootServletInitializer {
	
	@Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(SpringbootApplication.class);
    }

	public static void main(String[] args) {
		SpringApplication.run(SpringbootApplication.class, args);

	}

}
