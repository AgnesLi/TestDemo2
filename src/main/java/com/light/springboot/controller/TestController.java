package com.light.springboot.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TestController {
	@GetMapping("/helloword")
	public String helloword() {
		return "helloword";
	}
	
	@GetMapping("/helloword3")
	public String helloword3() {
		return "helloword3";
	}
}
